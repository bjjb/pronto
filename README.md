pronto
======

A nano-second-capable [cron][1]/[at][2] alternative.

[1]: https://en.wikipedia.org/wiki/Cron
[2]: https://en.wikipedia.org/wiki/At_(command)
